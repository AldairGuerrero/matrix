(function () {
  'use strict';
  angular.module('matrix').directive('categoria', categoria);
  categoria.$inject = ['$rootScope','$compile'];
  function  categoria($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/categoria.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'categoriaController'
    };
  }
  angular.module('matrix').directive('estadodocumento', estadodocumento);
  estadodocumento.$inject = ['$rootScope','$compile'];
  function  estadodocumento($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/estadodocumento.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'estadodocumentoController'
    };
  }
  angular.module('matrix').directive('origen', origen);
  origen.$inject = ['$rootScope','$compile'];
  function  origen($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/origen.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'origenController'
    };
  }
  angular.module('matrix').directive('tipodocumento', tipodocumento);
  tipodocumento.$inject = ['$rootScope','$compile'];
  function  tipodocumento($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/tipodocumento.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'tipodocumentoController'
    };
  }
  angular.module('matrix').directive('origendocumentos', origendocumentos);
  origendocumentos.$inject = ['$rootScope','$compile'];
  function  origendocumentos($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/origendocumentos.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'origendocumentosController'
    };
  }
  angular.module('matrix').directive('motivo', motivo);
  motivo.$inject = ['$rootScope','$compile'];
  function  motivo($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/motivo.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'motivoController'
    };
  }
  angular.module('matrix').directive('submotivo', submotivo);
  submotivo.$inject = ['$rootScope','$compile'];
  function  submotivo($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/documento/submotivo.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'submotivoController'
    };
  }
})();
