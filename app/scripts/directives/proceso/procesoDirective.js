(function () {
  'use strict';
  angular.module('matrix').directive('recepcion', recepcion);
  recepcion.$inject = ['$rootScope','$compile'];
  function  recepcion($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/proceso/recepcion.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'recepcionController'
    };
  }
  angular.module('matrix').directive('cargue', cargue);
  cargue.$inject = ['$rootScope','$compile'];
  function  cargue($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/proceso/cargue.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'cargueController'
    };
  }
  angular.module('matrix').directive('entrega', entrega);
  entrega.$inject = ['$rootScope','$compile'];
  function  entrega($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/proceso/entrega.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'entregaController'
    };
  }
  angular.module('matrix').directive('retencion', retencion);
  retencion.$inject = ['$rootScope','$compile'];
  function  retencion($rootScope,$compile){
    return {
      restrict: 'EA',
      templateUrl: 'app/views/proceso/retencion.html',
      replace: true,
      link: function(scope, element) {
        $compile(element.contents())($rootScope);
      },
      controller:'retencionController'
    };
  }
})();
