'use strict';

/**
 * @ngdoc overview
 * @name matrix
 * @description
 * # GenesisApp
 *
 * Main module of the application.
 */
angular
  .module('matrix', [
      'ui.router'
    ])
  .factory('comunication', function() {
    return {
      message: 'vacio',
      getMessage: function() {
        return this.message;
      },
      setMessage: function(msg) {
        this.message = msg;
      }
    };
  })
  .config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
    $locationProvider.html5Mode(false);
    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('matrix', {
      url: "/",
      abstract: true,
      template: '<ui-view/>'
    })
    .state('matrix.inicio', {
      url:'Inicio',
      template: '<div inicio></div>'
    })
    .state('matrix.empresa', {
      url:'Empresa',
      template: '<div empresa></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.sede', {
      url:'Sede',
      template: '<div sede></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.oficina', {
      url:'Oficina',
      template: '<div oficina></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.area', {
      url:'Area',
      template: '<div area></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.cargo', {
      url:'Cargo',
      template: '<div cargo></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.mensajeria', {
      url:'Mensajeria',
      template: '<div mensajeria></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.rol', {
      url:'Rol',
      template: '<div rol></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.usuario', {
      url:'Usuario',
      template: '<div usuario></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.categoria', {
      url:'Categorias',
      template: '<div categoria></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.estadodocumento', {
      url:'EstadoDocumento',
      template: '<div estadodocumento></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.origen', {
      url:'Origen',
      template: '<div origen></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.tipodocumento', {
      url:'TipoDocumentos',
      template: '<div tipodocumento></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.origendocumentos', {
      url:'OrigenDocumentos',
      template: '<div origendocumentos></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.motivo', {
      url:'Motivo',
      template: '<div motivo></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.submotivo', {
      url:'Submotivo',
      template: '<div submotivo></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.recepcion', {
      url:'Recepcion',
      template: '<div recepcion></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.cargue', {
      url:'Cargue',
      template: '<div cargue></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.entrega', {
      url:'Entrega',
      template: '<div entrega></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.retencion', {
      url:'Retencion',
      template: '<div retencion></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.dashboard', {
      url:'Dashboard',
      template: '<div dashboard></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.tiempo', {
      url:'Tiempo',
      template: '<div tiempo></div>',
      data: {'matrix':'Matrix'}
    })
    .state('matrix.bi', {
      url:'Bi',
      template: '<div bi></div>',
      data: {'matrix':'Matrix'}
    })

  });
